﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElasticSearch.Data.Entities
{
    public class Pedidos
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public int FilialId { get; set; }
        public string Filial { get; set; }
        public int ClienteId { get; set; }
        public string Cliente { get; set; }
        public int VendedorId { get; set; }
        public string Vendedor { get; set; }
        public int ProdutoId { get; set; }
        public string Produto { get; set; }
        public int TipoCulturaId { get; set; }
        public string TipoCultura { get; set; }
        public int GrupoProdutoId { get; set; }
        public string GrupoProduto { get; set; }
        public double QtdUnitario { get; set; }
        public double ValorUnitario { get; set; }
        public double ValorTotal { get; set; }
        public double PesoTotal { get; set; }
        public string Status { get; set; }
    }
}

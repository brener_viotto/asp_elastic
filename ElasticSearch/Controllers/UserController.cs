﻿using Dapper;
using ElasticSearch.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nest;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearch.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {


        private readonly IElasticClient _elasticClient;
        public UserController(IElasticClient elasticClient)
        {
            _elasticClient = elasticClient;
        }

        //Faz uma gravação no banco

        [HttpPost]
        public async Task<string> post(User value)
        {
            var response = await _elasticClient.IndexAsync<User>(value, x => x.Index("users"));
            return response.Id;
        }

        // Lista um item do banco pelo id
        [HttpGet("{id}")]
        public async Task<User> get(string id)
        {
            var response = await _elasticClient.GetAsync<User>(id);

            return response.Source;

        }

        // Lista todos os dados do banco
        [HttpGet("All")]
        public async Task<ICollection<User>> getAll()
        {
            var result = await _elasticClient.SearchAsync<User>(s => s
           .Index("users")
           .Size(10)
           .Sort(q => q.Ascending(p => p.Id)));

            return result.Documents.ToList();



        }
        //Deleta dados do banco pelo id 

        [HttpDelete("{id}")]
        public async Task<string> destroy(string id)
        {
            var result = await _elasticClient.DeleteAsync<User>(id);
            if (result.IsValid)
            {
                return "Deletado com sucesso";
            }

            return "Algo deu errado";
        }

        //Atualiza dados do banco pelo id 
        [HttpPut("{id}")]
        public async Task<string> update(string id, User user)
        {

            var result = await _elasticClient.UpdateAsync<User, dynamic > (new DocumentPath<User>(id), u => u.Index("users").Doc(user));

            if (result.IsValid)
            {
                return "Atualizado";
            }
            return "erro";
        }

        [HttpGet("pedidos")]
        public async Task<ICollection<Pedidos>> pedidos()
        {

            var result = await _elasticClient.SearchAsync<Pedidos>(s => s
           .Index("pedidos")
           .Size(6313)
           .Sort(q => q.Ascending(p => p.Id)));

            return result.Documents.ToList();
        }

        [HttpGet("sincronizacao")]
        public async void sincronizacao()
        {

            using (var con = new SqlConnection("Server=52.161.0.22;Database=A2W_Dfe_Prod; User ID=sa; Password=$Acesso@01!!"))
            {
                
                con.Open();

                string query = $@"select * from (
                    select
                    nf.Id as Id, 
                    nf.dataLancamento as Data,
                    e.Id as FilialId,
                    e.nomeFantasia as Filial,
                    pn.Id as ClienteId,
                    pn.nomeRazaoSocial as Cliente,
                    v.Id as VendedorId,
					v.nomeVendedor as Vendedor,
                    p.Id as ProdutoId,
                    p.descricaoProduto as produto,
                    tc.Id as TipoCulturaId,
                    tc.descricao as TipoCultura,
                    gp.Id as GrupoProdutoId,
                    gp.descricao as GrupoProduto,
                    nfi.qtdItemNota as QtdUnitario,
                    nfi.valorUnitNota as ValorUnitario,
                    (nfi.qtdItemNota * nfi.valorUnitNota) as ValorTotal,
                    (nfi.pesoUnidadeMedidaVendaItem * nfi.qtdItemNota) as PesoTotal,
                    nf.statusMovimento as Status
                    from NotaFiscal as nf inner join NotaFiscalItens as nfi on nfi.notaFiscalId = nf.Id 
                    inner join ParceiroNegocio as pn on pn.Id = nf.ParceiroNegocioId
                    inner join dbo.Empresa as e on e.Id = nf.empresaEmitenteId
                    inner join Vendedor as v on v.Id = nf.vendedorPrincipalId
                    inner join Produto as p on nfi.produtoId = p.Id
                    inner join TipoCultura as tc on tc.Id = nfi.tipoCulturaId
                    inner join GrupoProduto as gp on gp.Id = p.grupoProdutoId
                    ) as tbl";

                var list = await con.QueryAsync<Pedidos>(query);
                con.Close();

                foreach (var item in list)
                {

                    var response = await _elasticClient.IndexAsync<Pedidos>(item, x => x.Index("pedidos"));

                    
                }


                


                


                


            }
        }


    }
}
